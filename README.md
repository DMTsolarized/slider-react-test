# Slider application for Scandiweb

## Dependencies:

* React
* React-dom

## To set-up:

* git clone https://DMTsolarized@bitbucket.org/DMTsolarized/slider-react-test.git
* cd slider-react-test
* npm ci 
* npm start

## Changelog:
* fixed blinking images (I hope)
* finger-following swipes on desktop
* animated movement when using dots
## All the other information is displayed as comments inside the code.
