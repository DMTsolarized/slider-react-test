import React, { useEffect, useRef, useState } from 'react'
import './style/slider.scss'
import Page from './Page'
function Slider(props) {
	//slider uses only 3 slides - current previous and the next one
	let slideContent = props.children
	const firstSlide = slideContent[0]
	const secondSlide = slideContent[1]
	const lastSlide = slideContent[slideContent.length - 1]
	const [SlideData, setSlideData] = useState({
		activeSlide: 0,
		translate: 100, // we display the middle slide so translate should be 100 at the start
		transition: 0.4,
		slides: [lastSlide, firstSlide, secondSlide],
	})
	const { activeSlide, translate, slides, transition } = SlideData
	const slideChange = useRef(false)
	//we check for transition at every render so when it happens we can generate new array of 3 slides
	const transitionCheck = useRef()
	useEffect(() => {
		transitionCheck.current = updatedSlides
	})
	useEffect(() => {
		const transition = () => {
			transitionCheck.current()
		}
		const transitionEnd = window.addEventListener('transitionend', transition) //when transition ends it updates the array
		return () => {
			window.removeEventListener('transitionend', transitionEnd)
		}
	}, [])

	const updatedSlides = () => {
		if (!slideChange.current) return
		slideChange.current = false
		setSlideData({
			...SlideData,
			slides: slideUpdated(activeSlide),
			translate: 100,
			transition: 0,
		})
	}
	useEffect(() => {
		//after swapping we need to reset the transition time
		if (transition === 0) setSlideData({ ...SlideData, transition: 0.4 })
	}, [transition])
	const slideUpdated = slideIndex => {
		//generation of the slides array
		let slides = []
		if (slideIndex === slideContent.length - 1)
			slides = [slideContent[slideContent.length - 2], lastSlide, firstSlide]
		else if (slideIndex === 0) slides = [lastSlide, firstSlide, secondSlide]
		else slides = slideContent.slice(slideIndex - 1, slideIndex + 2)
		return slides
	}
	const goRight = () => {
		slideChange.current = true
		setSlideData({
			...SlideData,
			translate: 200,
			activeSlide:
				activeSlide === slideContent.length - 1 ? 0 : activeSlide + 1,
		})
	}
	const goLeft = () => {
		slideChange.current = true
		setSlideData({
			...SlideData,
			translate: 0,
			activeSlide:
				activeSlide === 0 ? slideContent.length - 1 : activeSlide - 1,
		})
	}
	const goSelected = newSlide => {
		if (newSlide == activeSlide) return
		if (newSlide == activeSlide + 1) {
			goRight()
			return
		}
		slideChange.current = true
		let _newSlide = newSlide
		let reverse = false
		if (newSlide > activeSlide) newSlide++
		else {
			reverse = true
		}

		let _slides = slideContent.slice(
			Math.min(activeSlide, newSlide),
			Math.max(activeSlide, newSlide)
		)
		let translateDistance = _slides.length - 1
		if (reverse) translateDistance = 0
		setSlideData({
			slides: _slides,
			translate: 100 * translateDistance,
			transition: 0.6,
			activeSlide: _newSlide,
		})
	}
	//variables for touch distance calculations
	const lastTouch = useRef()
	let delta = 0
	const HandleTouchStart = e => {
		lastTouch.current = e.nativeEvent.touches[0].clientX
	}
	const HandleTouchMovement = e => {
		delta = lastTouch.current - e.nativeEvent.touches[0].clientX
		lastTouch.current = e.nativeEvent.touches[0].clientX
		delta /= 4
		if (translate + delta > 200 || translate + delta < 0) return
		setSlideData({ ...SlideData, translate: translate + delta, transition: 0 })
	}
	const HandleTouchEnd = () => {
		if (translate >= 115) goRight()
		else if (translate <= 75) goLeft()
		else setSlideData({ ...SlideData, translate: 100, transition: 0.4 })
	}
	//variables for click distance calculations
	const click = useRef(false)
	let deltaClick = 0
	const ClickPosition = useRef(0)
	const HandleMouseDown = e => {
		click.current = true //registering click initialization
		ClickPosition.current = e.clientX
	}
	const HandleMouseMove = e => {
		if (!click.current) return
		deltaClick = ClickPosition.current - e.nativeEvent.clientX
		ClickPosition.current = e.nativeEvent.clientX
		deltaClick /= 5
		setSlideData({ ...SlideData, translate: translate + deltaClick, transition: 0 })
	}
	const HandleMouseUp = () => {
		if (!click.current) return
		click.current = false
		if (translate >= 115) goRight()
		else if (translate <= 75) goLeft()
		else setSlideData({ ...SlideData, translate: 100, transition: 0.4 })
	}
	//checking if mouse went out of slider div
	const HandleMouseOut = e => {
		if (!click.current) return
		click.current = false
		if (e.clientX < 500) goRight()
		else goLeft()
	}
	return (
		<div
			id="slider"
			onTouchStart={HandleTouchStart}
			onTouchMove={HandleTouchMovement}
			onTouchEnd={HandleTouchEnd}
			onMouseMove={HandleMouseMove}
			onMouseUp={HandleMouseUp}
			onMouseDown={HandleMouseDown}
			onMouseOut={HandleMouseOut}
		>
			<Page slides={slides} translate={translate} transition={transition} />
			<button id="Left" onClick={goLeft}>
				←
			</button>
			<button id="Right" onClick={goRight}>
				→
			</button>
			<div id="dots-container">
				{slideContent.map((item, key) => {
					return (
						<span
							id="dot"
							key={key}
							index={key}
							style={{ background: key == activeSlide ? 'black' : 'gray' }}
							onClick={e =>
								goSelected(parseInt(e.target.getAttribute('index')))
							}
						/>
					)
				})}
			</div>
		</div>
	)
}

export default Slider
/*
 */
