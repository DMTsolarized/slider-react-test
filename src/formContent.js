import React from 'react'

const FormContent = () => {
	const formStyle = {
		width: '100%',
		height: '100%',
	}
	return (
		<div id="form-container" style={formStyle}>
			<form>
				<label>
					name: <br />
					<input type="text" id="name" name="name" /> <br />
				</label>
				<label>
					surname: <br />
					<input type="text" id="surname" name="surname" />
				</label>
				<br />
				<label>
					give us your feedback <br />
					<textarea name="feedback" rows="5" cols="10" />
				</label>{' '}
				<br />
				<input type="submit" />
			</form>
		</div>
	)
}

export default FormContent
