import React from 'react'

const ImgContent = props => {
	return (
		<div className="img-div" style={{backgroundImage: `url(${props.content})`}}>
		</div>
		)
}

export default ImgContent
