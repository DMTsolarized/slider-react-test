import React from 'react'
import ReactDOM from 'react-dom'
import Slider from './Slider'
import FormContent from './formContent'
import ImgContent from './imageContent'
/*slider takes in an array for content so array can be populated using ready components
  I have two different types of content image and form ImgContent component takes url of the image 
  as an argument. 
*/
ReactDOM.render(
	<Slider>
		<ImgContent
		content={
			'https://images.pexels.com/photos/620337/pexels-photo-620337.jpeg?cs=srgb&dl=pexels-tobi-620337.jpg&fm=jpg'
		}
	/>
	<ImgContent
		content={
			'https://upload.wikimedia.org/wikipedia/commons/2/21/Adams_The_Tetons_and_the_Snake_River.jpg'
		}
	/>
	<ImgContent
		content={
			'https://ipt.imgix.net/205939/x/0/.jpg?w=860&h=480&fit=crop&auto=format%2C%20compress&dpr=2&ixlib=react-8.6.4'
		}
	/>
	4
	<FormContent />
	
		</Slider>,
	document.getElementById('root')
)
