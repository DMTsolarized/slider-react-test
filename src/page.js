import React from 'react'
import Slide from './Slide'

const Page = props => {
	const divStyle = {
		transform: `translateX(-${props.translate}%)`,
		transition: `${props.transition}s`,
	}
	return (
		<div className="container" style={divStyle}>
			{props.slides.map((item, index) => {
				return <Slide key={index}>
						{item}
					</Slide>
			})}
		</div>
	)
}

export default Page
