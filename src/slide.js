import React from 'react'

const Slide = props => {
	return(
	<div className="slide">{props.children}</div>
	)
}
export default Slide
